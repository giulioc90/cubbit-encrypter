import styled from "styled-components";

export const CenteredContentContainer = styled.section`
    text-align: center; 
`
export const Paragraph = styled.p`
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 26px;
    text-align: center;
    margin-top: 20px;
`
export const BlackParagraph = styled(Paragraph)`
    color: #161616;
`
export const WhiteParagraph = styled(Paragraph)`
    color: white;
`
export const MainButton = styled.button`
    height: 48px;
    width: 168px;
    -webkit-box-pack: center;
    justify-content: center;
    color: white;
    background-color: #009EFF;
    opacity: 1;
    cursor: pointer;
    display: flex;
    -webkit-box-align: center;
    align-items: center;
    white-space: pre-wrap;
    padding: 0.5rem 0px;
    margin: auto 0.5rem;
    border-radius: 3px;
    border-style: solid;
    border-color: transparent;
    border-image: initial;
    text-decoration: none;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 26px;
    text-align: center;

    &:hover {
      opacity: 0.9 // <Thing> when hovered
    } 
`

export const SecondaryButton = styled(MainButton)`
  background-color: #FFA047;;
`
