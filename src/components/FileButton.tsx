import React from 'react'
import styled from "styled-components";
import arrowIcon from "../assests/Arrow.svg";
import fileIcon from "../assests/File.svg";

interface ButtonProps {
    id: string;
    onClick: (id: string) => void;
    text: string;
}

interface ButtonStyleProp {
    border: boolean;
}
const FileButtonContainerStyled = styled.div`    
    display: flex;
    justify-content: center;
`
const FileButtonWrapperStyled = styled.div`    
    border-radius: 3px;
    background-color: white;
    height: 48px;
    width: 220px;
    display: flex;
    justify-content: center;
    padding: 2px;
`

const ButtonStyled = styled.button`
    border: 0;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    background-color: white;  
    border-left: ${(props : ButtonStyleProp) => props.border ? "1px solid #98A0A6" : "0px"}; 
`
const ButtonIcon = styled.img`
    width: 19px;
    height: 22px;
    margin:auto;
`

function FileButton(props: ButtonProps){
    return (
        <FileButtonContainerStyled >
            <FileButtonWrapperStyled>
                <ButtonIcon src={fileIcon} alt=""/>
                <ButtonStyled border={false}>{props.text}</ButtonStyled>
                <ButtonStyled border={true}>
                    <img src={arrowIcon} alt=""/>
                </ButtonStyled>
            </FileButtonWrapperStyled>


        </FileButtonContainerStyled>
    )
}

export default FileButton