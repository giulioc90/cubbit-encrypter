import React from "react";
import styled from "styled-components";
import {useDispatch, useSelector } from 'react-redux';
import {toggle, selectLanguage} from "../store/EncrypterSlice"
import {t} from "../Localizator";

const StyledInput = styled.div`
    
    position: relative;
    top: 50%;
    left: 50%;
    overflow: hidden;
    -webkit-transform: translate3d(-50%, -50%, 0);
    transform: translate3d(-50%, -50%, 0);
    margin-right: 160px;
    
    input{
        display: none;
    }
    .toggle{
        cursor: pointer;
        display: flex;
        position: relative;
        width: 240px;
        height: 35px;
        background: #009EFF;
        border-radius: 3px;
        -webkit-transition: all 200ms cubic-bezier(0.445, 0.05, 0.55, 0.95);
        transition: all 200ms cubic-bezier(0.445, 0.05, 0.55, 0.95);
    }
    
    .choice_label{
        position: absolute;
        z-index: 2;
        width: 120px;
        height: 35px;
        text-align: center;
        font-size: 14px;
        align-items: center;
        margin-top: 7px;
    }
    
    .right_choice{
        right: 0;
    }
   
    .toggle__handler{
        position: relative;
        z-index: 1;
        background: #363636;
        width: 120px;
        height: 31px;
        border-radius: 3px;
        top: 2px;
        left: 2px;
        -webkit-transition: all 200ms cubic-bezier(0.445, 0.05, 0.55, 0.95);
        transition: all 200ms cubic-bezier(0.445, 0.05, 0.55, 0.95);
        -webkit-transform: translateX(0px);
        transform: translateX(0px);
    }
   
    input:checked + .toggle .toggle__handler{
        width: 116px;
        -webkit-transform: translateX(120px);
        transform: translateX(120px);
        border-color: #fff;
    }
    
`

function LanguageSwitch(){
    let selectorLanguage = useSelector(selectLanguage);
    const dispatch = useDispatch()
    const label = t("W\"y {'z", selectorLanguage);

    return (
        <StyledInput>
            <input type="checkbox" id="dn" className="dn" onChange={() => dispatch(toggle())}/>
            <label htmlFor="dn" className="toggle">
                <span className="choice_label">Encrypted</span>
                <span className="choice_label right_choice">{label}</span>
                <span className="toggle__handler"/>
            </label>
        </StyledInput>
    )
}

export default LanguageSwitch