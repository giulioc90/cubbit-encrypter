import React, {useCallback, useState} from 'react';
import styled from "styled-components";
import {useDropzone} from 'react-dropzone'
import FileButton from "./FileButton";
import fileIcon from "../assests/File.svg";
import {BlackParagraph, CenteredContentContainer} from "../style/CommonStyles";
import {selectLanguage} from "../store/EncrypterSlice";
import { useSelector } from 'react-redux';
import {t} from "../Localizator";

const DropZoneSection = styled.section`
    background-color: #FFA047; 
    height: 216px;
    position: relative;
    margin: auto;    
`

const InternalDropZoneSection = styled.div`
    position: absolute;
    background: rgba(22, 22, 22, 0.16);
    border: 1px dashed #363636;
    box-sizing: border-box;
    left: 0.85%;
    right: 0.85%;
    top: 3.7%;
    bottom: 3.7%;
    display: flex;
    justify-content: center;
    flex-direction: column;
`

interface FileUploaderProps{
    onFileSelected: (file: File) => void;
}

function FileUploader(props: FileUploaderProps){
    const [selectedFileUrl, setSelectedFileUrl] = useState("");

    const onDrop = useCallback(acceptedFiles => {
        const file = acceptedFiles[0];
        setSelectedFileUrl(file.name);
        props.onFileSelected(file);
    }, []);

    const { getRootProps, getInputProps} = useDropzone({onDrop});
    let buttonLabel = t("Uz##'w2x{ w3", useSelector(selectLanguage));

    return (
        <DropZoneSection >
            <InternalDropZoneSection {...getRootProps()}>
                <input {...getInputProps()} />

                {
                    selectedFileUrl === ""
                        ?   <CenteredContentContainer>
                                <FileButton  id="id" onClick={id => console.log(id)} text={buttonLabel}/>
                                <BlackParagraph>or drop files here</BlackParagraph>
                            </CenteredContentContainer>
                        :
                            <CenteredContentContainer>
                                <img src={fileIcon} alt={selectedFileUrl} />
                                <BlackParagraph>{selectedFileUrl}</BlackParagraph>
                            </CenteredContentContainer>
                }

            </InternalDropZoneSection>
        </DropZoneSection>
    );
}

export default FileUploader;