import React, {useState} from 'react';
import styled from 'styled-components';
import FileUploader from './FileUploader';
import Enigma from '@cubbit/enigma';
import {WebFileStream} from '@cubbit/web-file-stream';
import {saveAs} from 'file-saver';
import fileIcon from "../assests/FileLogoWhite.svg";
import {CenteredContentContainer, MainButton, SecondaryButton, WhiteParagraph} from "../style/CommonStyles";
import {selectLanguage} from "../store/EncrypterSlice";
import { useSelector } from 'react-redux';
import {t} from "../Localizator";

const copy = require('copy-text-to-clipboard');

const SubTitle = styled.p`
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 26px;
    text-align: center;
    min-height: 70px;
`

const FileManagerContainer = styled.section`
    margin: auto;
`

const ButtonsContainer = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 42px;
`

const ResultFileBox = styled.div`
    border: 1px solid #363636;
    box-sizing: border-box;
    border-radius: 3px;
    padding: 30px;
`

const StyledForm = styled.div`
    display:flex;
    flex-direction:row;
    background: #292929;
    border: 1px solid #363636;
    box-sizing: border-box;
    border-radius: 3px;
    
    input {
        flex-grow:2;
        border:none;
        background: #292929;
        color: white;
        margin-right: 32px;
        margin-left: 32px;
        font-size: 14px;
        line-height: 23px;
    }
    
    input:focus {
        outline: none;
    }
    
    button {
        height: 36px;
        width: 116px;
        margin: 6px 6px;
    }
`


enum Navigation {
    Main ,
    Encrypter,
    Decrypter
}

function FileManager(){

    const [aesKey, setAesKey] = useState<string>();
    const [selectedFile, setSelectedFile] = useState<File>();
    const [blobFile, setBlobFile] = useState<Blob>();

    const [page, setPage] = useState<Navigation>(Navigation.Main);

    async function startEncrypting() {
        if(selectedFile){
            await Enigma.init();
            const read_stream = WebFileStream.create_read_stream(selectedFile);
            const key = Enigma.AES.create_key();
            const aes = await new Enigma.AES().init({key: key});
            const iv = Enigma.Random.bytes(16);

            console.log(iv);
            const aes_stream = aes.encrypt_stream(iv);

            let enc_buffer = Buffer.alloc(0);
            aes_stream.once('finish', () => {
                console.log("QUI");
                const data = aes_stream.read() as Buffer;
                if(data)
                    enc_buffer = Buffer.concat([iv, enc_buffer, data]);
                setBlobFile(new Blob([enc_buffer]));
            });

            read_stream.pipe(aes_stream);

            setAesKey(key.toString("base64"));
            setPage(Navigation.Encrypter);
        }else{
            alert("A file must be selected!")
        }
    }
    async function startDecrypting() {
        if(selectedFile && aesKey){

            const vectorStream = WebFileStream.create_read_stream(selectedFile, { end: 16 });
            let iv: Buffer;
            vectorStream.on('data', (chunk) => {
                iv = chunk;
            });
            vectorStream.on('error', console.error);
            vectorStream.on('end', async () => {
                console.log(iv);
                await Enigma.init();

                const read_stream = WebFileStream.create_read_stream(selectedFile, { start: 16 });
                try{
                    const aes = await new Enigma.AES().init({key: new Buffer(aesKey, 'base64')});
                    const aes_stream = aes.decrypt_stream(iv);

                    let dec_buffer = Buffer.alloc(0);
                    aes_stream.once('finish', () => {
                        const data = aes_stream.read() as Buffer;
                        if(data)
                            dec_buffer = Buffer.concat([dec_buffer, data]);
                        saveAs(new Blob([dec_buffer]), "decrypted_"+selectedFile.name.replace("encrypted_", ""))
                    });
                    aes_stream.once('error', (error) => {
                        console.log(error);
                    })
                    read_stream.pipe(aes_stream);
                }catch (e) {
                    alert(e);
                }
            });
        }
    }

    function downloadFile(){
        if(blobFile && selectedFile){
            saveAs(blobFile, "encrypted_"+selectedFile.name);
        }else{
            alert("The File must be encrypted")
        }

    }
    function copyKeyToClipboard() {
        copy(aesKey);
        alert("Copied to clipboard");
    }

    let subTitle = t("Sv*s\"uwv2#\" {\"w2x{ w2w\"u&-$({#\"2s\"v2vwu&-$({#\"@2ewu)&w2s\"-2x{ w2(-$w2s\"v2!s{\"(s{\"2-#)&2$&{*su-3", useSelector(selectLanguage));

    return(
        <FileManagerContainer>
        {
            page === Navigation.Encrypter ?
                <CenteredContentContainer>
                    <ResultFileBox>
                            <img src={fileIcon} alt="{selectedFile?.name}" />
                            <WhiteParagraph>{selectedFile?.name}</WhiteParagraph>
                    </ResultFileBox>
                    <WhiteParagraph>Your encryption key:</WhiteParagraph>
                    <StyledForm>
                        <input readOnly={true} value={aesKey}/>
                        <MainButton id="" onClick={() => copyKeyToClipboard()}>Copy</MainButton>
                    </StyledForm>
                    <ButtonsContainer>
                        <SecondaryButton onClick={() => downloadFile()}>Download</SecondaryButton>
                    </ButtonsContainer>
                </CenteredContentContainer>
            : page === Navigation.Decrypter ?
                <CenteredContentContainer>
                    <ResultFileBox>
                        <img src={fileIcon} alt="{selectedFile?.name}" />
                        <WhiteParagraph>{selectedFile?.name}</WhiteParagraph>
                    </ResultFileBox>
                    <WhiteParagraph>Insert your key:</WhiteParagraph>
                    <StyledForm>
                        <input type="text" onChange={(event) => {setAesKey(event.target.value)}}/>
                    </StyledForm>
                    <ButtonsContainer>
                        <SecondaryButton onClick={() => startDecrypting()}>Decrypt and Download</SecondaryButton>
                    </ButtonsContainer>
                </CenteredContentContainer>
            :
                <div>
                    <SubTitle>{subTitle}</SubTitle>
                    <FileUploader onFileSelected={(file: File) => setSelectedFile(file)}/>
                    <ButtonsContainer>
                        <MainButton onClick={() => startEncrypting()}>Encrypt</MainButton>
                        <MainButton onClick={() => setPage(Navigation.Decrypter)}>Decrypt</MainButton>
                    </ButtonsContainer>
                </div>
        }
        </FileManagerContainer>
    )
}

export default FileManager;