import React from "react";
import styled from "styled-components";
import FileManager from "./FileManager";
import {selectLanguage} from "../store/EncrypterSlice";
import {useSelector} from 'react-redux';
import {t} from "../Localizator";
import {Paragraph} from "../style/CommonStyles";

const Container = styled.div`
      background-color: #161616;
      flex: 1 1 auto;
      justify-content: center;
`

const TitleSection = styled.section`
    font-style: normal;
    font-weight: 900;
    font-size: 36px;
    line-height: 47px;
    text-align: center;
    margin:auto;
`

const ContainerBox = styled.div`
    margin-left: 20%;
    margin-right: 20%;
`

const Footer = styled.div`
    position: absolute;
    bottom: 10px;
    width: 100%;
`

function ContentContainer(){


    const footerText = t("fzw2+z# w2{'2\"w*w&2(zw2')!2#x2(zw2$s&('2?2{(2{'2y&ws(w&2#&2 w''w&>2vw$w\"v{\"y2#\"2z#+2+w  2(zw2{\"v{*{v)s '2+#&}2(#yw(zw&", useSelector(selectLanguage))

    let title = t("U)tt{(2w\"u&-$(#&", useSelector(selectLanguage));

    return(
        <Container>
            <ContainerBox>
                <TitleSection>
                    <h2>{title}</h2>
                </TitleSection>
                <FileManager/>
            </ContainerBox>
            <Footer>
                <Paragraph>{footerText}</Paragraph>
            </Footer>
        </Container>
    )
}

export default ContentContainer