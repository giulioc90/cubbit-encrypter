import React from 'react';
import logo from '../assests/Logo.png';
import styled from "styled-components";
import LanguageSwitch from "./LanguageSwitch";

const StyledHeader = styled.header`
    flex: 0 1 3.4rem;
    width: 100%;
    background-color: rgb(41, 41, 41);
    z-index: 15;
    box-shadow: rgba(4, 4, 4, 0.4) 0px 1px 2px;
    box-sizing: border-box;
    grid-area: header / header / header / header;
    border-width: 1px;
    border-style: solid;
    border-color: #292929;
    border-image: initial;
`

const HeaderWrapper = styled.div`
    height: 100%;
    position: relative;
    display: flex;
    -webkit-box-align: center;
    align-items: center;
`

const Logo = styled.img`
    z-index: 5;
    height: 2.5rem;
    cursor: pointer;
    margin: auto 0.8rem;
`

const SwitchWrapper = styled.div`
    margin-left: auto;
    height: 100%;
    
`
function Header() {
    return(
        <StyledHeader>
            <HeaderWrapper>
                <Logo
                    src={logo}
                />
                <SwitchWrapper>
                    <LanguageSwitch/>

                </SwitchWrapper>

            </HeaderWrapper>

        </StyledHeader>
    );
}

export default Header