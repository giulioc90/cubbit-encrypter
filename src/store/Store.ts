import { configureStore } from '@reduxjs/toolkit';
import languageReducer from "./EncrypterSlice"

export const store = configureStore({
    reducer: {
        localization: languageReducer,
    },
});

export type RootState = ReturnType<typeof store.getState>;
