import { createSlice } from '@reduxjs/toolkit';
import { RootState } from './Store';

interface LanguageState {
    isEncrypted: boolean;
}

const initialState: LanguageState = {
    isEncrypted: false,
};

export const languageSlice = createSlice({
    name: 'language',
    initialState,
    reducers: {
        toggle: state => {
            state.isEncrypted = !state.isEncrypted;
        }
    }
});
export const { toggle } = languageSlice.actions;
export const selectLanguage = (state: RootState) => state.localization.isEncrypted;
export default languageSlice.reducer;