import React from 'react';
import Header from "./components/Header";
import ContentContainer from "./components/ContentContainer";
import styled from "styled-components";
require('typeface-nunito');

function App() {

    const MainContainer = styled.div`
        display: flex;
        flex-flow: column;
        height: 100%;
        color: white;
        font-family: Nunito;
    `
    return (
        <MainContainer>
          <Header/>
          <ContentContainer/>
        </MainContainer>
    );
}

export default App;
