import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import {encrypt, decrypt} from "./Localizator";

test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

test('encrypter translator', () =>{
    let encryptedTitanicString = encrypt("frontend","titanic");
    expect(encryptedTitanicString).toBe('({(s"{u');

    let encryptedMinecraftString = encrypt("frontend","minecraft");
    expect(encryptedMinecraftString).toBe('!{"wu&sx(');

    let encryptedJavascriptString = encrypt("frontend","javascript");
    expect(encryptedJavascriptString).toBe('|s*s\'u&{$(');
})

test('decrypter translation', () =>{
    let decrypdedTitanic = decrypt("frontend", '({(s"{u');
    expect(decrypdedTitanic).toBe("titanic");

    let title = decrypt("frontend", "U)tt{(2w\"u&-$(#&");
    expect(title).toBe( "Cubbit encryptor");

})