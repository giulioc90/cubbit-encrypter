function chunkString(str: string, length: number) {
    return str.match(new RegExp('.{1,' + length + '}', 'g'));
}

function reverseString(str: string) {
    return str.split("").reverse().join("");
}

function sumASCII(str: string): number{
    let result = 0;
    for (let i = 0; i < str.length; i++) {
        result += str.charCodeAt(i);
    }
    return result;
}

export function encrypt(key: string, message: string): string{
    let chunks = chunkString(message, key.length);
    let shiftPosition = sumASCII(key);
    let result: string = "";
    if(chunks){
        for (const chunk of chunks) {
            let reversed = reverseString(chunk);
            let chunkResult = "";
            for (let i = 0; i < reversed.length; i++) {
                let baseCode: number = reversed.charCodeAt(i);
                let newCode: number = (baseCode - 31) + shiftPosition;
                newCode = (newCode % (94)) + 31;
                chunkResult += String.fromCharCode(newCode);
            }
            result += reverseString(chunkResult);
        }
    }else{
        result = message;
    }
    return  result;
}

export function decrypt(key: string, message: string): string{
    let chunks = chunkString(message, key.length);
    let shiftPosition = sumASCII(key);
    let result: string = "";
    if(chunks){
        for (const chunk of chunks) {
            let reversed = reverseString(chunk);
            let chunkResult = "";
            for (let i = 0; i < reversed.length; i++) {
                let baseCode: number = reversed.charCodeAt(i);
                let newCode: number = (baseCode - 31) - shiftPosition;
                newCode = ((newCode % 94) + 94) % 94 +31;
                chunkResult += String.fromCharCode(newCode);
            }
            result += reverseString(chunkResult);
        }
    }else{
        result = message;
    }
    return  result;
}

export function t(str: string, isToDecrypt: boolean): string {
    if(isToDecrypt){
        return decrypt("frontend", str);
    }else{
        return str;
    }

}